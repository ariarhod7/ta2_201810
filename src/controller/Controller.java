package controller;

import api.ITaxiTripsManager;
import model.data_structures.LinkedList;
import model.logic.TaxiTripsManager;
import model.vo.Taxi;
import model.vo.Service;

public class Controller {

	/**
	 * Reference to the services manager
	 */
	private static ITaxiTripsManager  manager = new TaxiTripsManager();
	
	public static void loadServices( ) throws Exception {
		// To define the dataset file's name 
		String serviceFile = "./data/taxi-trips-wrvz-psew-subset-small.json";
		
		manager.loadServices( serviceFile );
	}
		
	/**
	 * Ejecuta el metodo de busqueda de los taxiss que pertenecen a cierta compa�ia
	 * @param company compa�ia de la cual se van a buscar los taxis
	 * @return La lista d etaxis que pertenecen a la compa�ia ingresada como parametro
	 */
	public static LinkedList<Taxi> getTaxisOfCompany (String company) {
		return manager.getTaxisOfCompany(company);
	}
	
	/**
	 * Ejecuta el metodo de busqueda de servicios que terminan en el area ingresada como parametro 
	 * @param communityArea area de la cula se van a buscar los servicios que teeminen aho
	 * @return Una lista de servicios los cuales terminan en el area ingresada como parametro
	 */
	public static LinkedList<Service> getTaxiServicesToCommunityArea(int communityArea) {
		return manager.getTaxiServicesToCommunityArea( communityArea );
	}
}
