package model.logic;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;

import com.google.gson.stream.JsonReader;

import api.ITaxiTripsManager;
import model.vo.Taxi;
import model.vo.Service;
import model.data_structures.Iterador;
import model.data_structures.LinkedList;
import model.data_structures.ListaDoblementeEncadenada;

public class TaxiTripsManager implements ITaxiTripsManager {

	// TODO
	// Definition of data model 
	private ListaDoblementeEncadenada<Taxi> taxis;
	private ListaDoblementeEncadenada<Service> servicios;

	/**
	 * Este metodo carga los servicios y los taxis
	 */

	public void loadServices (String serviceFile) throws Exception {
		// TODO Auto-generated method stub
		System.out.println("Inside loadServices with " + serviceFile);
		JsonReader reader1 = null;
		try {
			File f = new File ( serviceFile);
			InputStream in = new FileInputStream(f);
			reader1 = new JsonReader(new InputStreamReader(in, "UTF-8"));
			servicios = (ListaDoblementeEncadenada<Service>) leerArrayServicios(reader1);
			taxis = cargarTaxi();


		} catch (FileNotFoundException | UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			try {
				reader1.close();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

		}


	}

	/**
	 * Este metodo indica que se va a leer el array de atributos de una instancia de Service
	 * @param reader1 JsonReader que se usara para leer el json
	 * @return retorna una ListaDoblementeEncadenada de los servicios del Json
	 */

	private LinkedList<Service> leerArrayServicios(JsonReader reader1) {
		// TODO Auto-generated method stub
		ListaDoblementeEncadenada<Service> ser = new ListaDoblementeEncadenada<>();
		try {
			reader1.beginArray();
			while ( reader1.hasNext()) {
				ser.add(leerServicio(reader1));
			}
			reader1.endArray();
			return ser;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Este metodo lee los atributos que se necesitan para contruir la clase Servicion
	 * @param reader1 JsonReader usado para leer la informacion dle Json
 	 * @return Una instancia de la clase Service
	 * @throws IOException
	 */
	private Service leerServicio(JsonReader reader1) throws IOException {
		// TODO Auto-generated method stub
		String idTrip = null;
		int drop = 0;
		String id = null;
		String compa�ia = null;
		reader1.beginObject();
		while (reader1.hasNext()) {
			String name = reader1.nextName();
			switch (name) {
			case "trip_id":
				idTrip = reader1.nextString();
				break;
			case "dropoff_community_area":
				drop = reader1.nextInt();
				break;
			case "company":
				compa�ia = reader1.nextString();
				break;
			case "taxi_id":
				id = reader1.nextString();
				break;
			default:
				reader1.skipValue();
				break;
			}
		}
		if ( compa�ia == null) {
			compa�ia = "Independiente";
		}
		reader1.endObject();
		return new Service(drop, idTrip, compa�ia, id);
	}

	/**
	 * Contruye una ListaDoblementeEncadenada con objetos de la clase Taxi usando lo servicios
	 * Ademas no se repiten taxis que tengan el mismo ID_TAXI
	 * @return Una lista doblemente encadenada con los objetos de tipo TAXI
	 * @throws Exception
	 */
	public ListaDoblementeEncadenada<Taxi> cargarTaxi () throws Exception {
		ListaDoblementeEncadenada<Taxi> retornado = new ListaDoblementeEncadenada<>();
		if ( servicios.size() > 0) {
			for ( int i = 0; i<servicios.size(); i++) {
				Taxi agregar = new Taxi (servicios.get(i).getCompa�ia(), servicios.get(i).getIdTaxi());
				if ( !retornado.contains(agregar)){
					retornado.add(agregar);
				}

			}
		}
		System.out.println(retornado.size());
		return retornado;
	}

	/**
	 * Obtiene todos los taxis que pertenecen a la compa�ia que entra como parametro
	 */
	@Override
	public LinkedList<Taxi> getTaxisOfCompany(String company) {
		// TODO Auto-generated method stub
		System.out.println("Inside getTaxisOfCompany with " + company);
		LinkedList<Taxi> retornado = new ListaDoblementeEncadenada<>();
		for ( int i = 0; i<taxis.size(); i++) {
			String uno = taxis.get(i).getCompany().replace(" ", "").trim();
			String dos = company.replace(" ", "").trim();
			if ( uno.equalsIgnoreCase(dos)) {
				try {
					retornado.add(taxis.get(i));
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return retornado;

	}

	/**
	 * Obtiene una lsita con todos los servicios que terminaron en la area de la ciudad que entra
	 * como paarametro
	 */
	@Override
	public LinkedList<Service> getTaxiServicesToCommunityArea(int communityArea) {
		// TODO Auto-generated method stub
		System.out.println("Inside getTaxiServicesToCommunityArea with " + communityArea);
		LinkedList<Service> retornado = new ListaDoblementeEncadenada<>();
		for ( int i = 0; i<servicios.size(); i++) {
			if ( servicios.get(i).getDropOffZone() == communityArea) {
				try {
					retornado.add(servicios.get(i));
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return retornado;
	}




}
