package model.vo;

/**
 * Representation of a taxi object
 */
public class Taxi implements Comparable<Taxi>{

	/**
	 * Atributos de la clase tAxi
	 */
	private String company;
	private String id;

	/**
	 * Contructor d ela clase taxi
	 * @param company Compa�ia a la que pertenece el taxi
	 * @param id cadena de caracteres que identifican el taxi
	 */
	public Taxi ( String company, String id) {
		this.company = company;
		this.id = id;
	}

	/**
	 * @return id 
	 */
	public String getTaxiId() {
		// TODO Auto-generated method stub
		return id;
	}

	/**
	 * @return company
	 */
	public String getCompany() {
		// TODO Auto-generated method stub
		return company;
	}

	/**
	 * Compara dos taxis por su ID
	 * @return 0 en caso que tengan el mismo id, 1 de lo contrario
	 */
	@Override
	public int compareTo(Taxi o) {
		// TODO Auto-generated method stub
		if ( this.getTaxiId().compareTo(o.getTaxiId()) == 0) {
			
			return 0;
		}
		else {
			return 1;
		}
	}	
	@Override
	public String toString() {
		return id;
	}
	
	
}
