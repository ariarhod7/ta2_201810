package model.vo;

/**
 * Representation of a Service object
 */
public class Service implements Comparable<Service> {

	/**
	 * Atributos de la clase Servicio
	 */
	private int dropOffZone; //Zona donde acaba el servicio
	private String idService; //Identificacion del servicio
	private String compa�ia; //Nombre de la compa�ia la cual ejecuto el servicio
	private String idTaxi; //Id del taxi que ejecuto el serviccio
	
	/**
	 * COntructor de la clase servicio
	 * @param drop zona donde acaba el servicio
	 * @param id identificacion del servicio
	 * @param compa�ia nombre d ela compa�ia que ejecuto el servicio
	 * @param idTaxi id del taxi que ejecuto el servicio
	 */

	public Service (int drop, String id, String compa�ia, String idTaxi) {
		dropOffZone = drop;
		idService = id;
		this.setCompa�ia(compa�ia);
		this.setIdTaxi(idTaxi);
	}
	/**
	 * Obtiene la zona en la que se acabo el servicio
	 * @return la zona en la que se acabo el servicio
	 */
	public int getDropOffZone() {
		return dropOffZone;
	}


	/**
	 * Retorna el id sel servicio
	 */
	public String getTripId() {
		// TODO Auto-generated method stub
		return idService;
	}	

	/**
	 * retoran el id del taxi que eejcuto el servicio
	 */
	public String getTaxiId() {
		// TODO Auto-generated method stub
		return idTaxi;
	}	

	/**
	 * @return time - Time of the trip in seconds.
	 */
	public int getTripSeconds() {
		// TODO Auto-generated method stub
		return 0;
	}

	/**
	 * @return miles - Distance of the trip in miles.
	 */
	public double getTripMiles() {
		// TODO Auto-generated method stub
		return 0;
	}

	/**
	 * @return total - Total cost of the trip
	 */
	public double getTripTotal() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int compareTo(Service o) {
		// TODO Auto-generated method stub
		return 0;
	}
	@Override
	public String toString() {
		return idService;
	}
	/**
	 * Obtiene la compa�ia la cual ejecuto el servicio
	 * @return
	 */
	public String getCompa�ia() {
		return compa�ia;
	}
	/**
	 * CAmbia la coma�ia la cual ejecuto el servicio
	 * @param compa�ia
	 */
	public void setCompa�ia(String compa�ia) {
		this.compa�ia = compa�ia;
	}
	/**
	 * Obtiene el id del taxi que ejecuto el servicio
	 * @return el id del taxi que ejecuto el servicio
	 */
	public String getIdTaxi() {
		return idTaxi;
	}
	/**
	 * CAmbia el id del taxiq ue eejcuto el servicio
	 * @param idTaxi el nuevo id del taxi que ejecuto el servicio
	 */
	public void setIdTaxi(String idTaxi) {
		this.idTaxi = idTaxi;
	}
}
