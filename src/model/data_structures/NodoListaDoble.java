package model.data_structures;

public class NodoListaDoble<T extends Comparable<T>> {
	
	/** 
	 * Atributos de la clase NodoListaDoble
	 */
	protected T elemento;
	protected NodoListaDoble<T> siguiente;
	protected NodoListaDoble<T> anterior;
	
	public NodoListaDoble(T elemento) {
		this.elemento = elemento;
		anterior = null;
		siguiente = null;
	}

	/**
	 * Ontiene el elemento guardado en el nodo
	 * @return El elemento guardado en el nodo
	 */
	public T getElemento() {
		return elemento;
	}

	/**
	 * Cambia el elemento guardado en el nodo
	 * @param elemento elemento que se quiere cambiar 
	 */
	public void setElemento(T elemento) {
		this.elemento = elemento;
	}

	/**
	 * Retorna el nodoSiguiente a Este nodo
	 * @return el nodoSiguiente
	 */
	public NodoListaDoble<T> getSiguiente() {
		return siguiente;
	}

	/**
	 * CAmbia el apuntador de este nodo por el ingresado como parametro
	 * @param siguiente El nuevo nodo al que se va a aputnar
	 */
	public void setSiguiente(NodoListaDoble<T> siguiente) {
		this.siguiente = siguiente;
	}

	/**
	 * Obtiene el nodo anterior a este nodo
	 * @return el nodo anterior a este nodo
	 */
	public NodoListaDoble<T> getAnterior() {
		return anterior;
	}

	/**
	 * CAmbia el apuntador del nodoAnterior por el nodo ingresado como parametro
	 * @param anterior el nodo al que se va apuntar como anterior
	 */
	public void setAnterior(NodoListaDoble<T> anterior) {
		this.anterior = anterior;
	}
	
	
	
	

}
