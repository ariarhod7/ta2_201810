package model.data_structures;

import java.util.ListIterator;

public class Iterador <T extends Comparable<T>> implements ListIterator<T> {
	
	/**
	 * Atributos de la clase iterador
	 */
	private NodoListaDoble<T> anterior;
	private NodoListaDoble<T> actual;
	
	/**
	 * Contructor de la clase iterador
	 * @param primerNodo
	 */
	public Iterador(NodoListaDoble<T> primerNodo) {
		actual = primerNodo;
		anterior = null;
	}

	@Override
	public void add(T arg0) {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException();
		
	}

	@Override
	public boolean hasNext() {
		// TODO Auto-generated method stub
		boolean retornado = false;
		if ( actual.getSiguiente() != null) {
			retornado = true;
		}
		return retornado;
	}

	@Override
	public boolean hasPrevious() {
		// TODO Auto-generated method stub
		boolean retornado = false;
		if ( actual.getAnterior() != null) {
			retornado = true;
		}
		return retornado;
	}

	@Override
	public T next() {
		// TODO Auto-generated method stub
		T retornado = null;
		actual = actual.getSiguiente();
		retornado = actual.getElemento();
		return retornado;
	}

	@Override
	public int nextIndex() {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException();
	}

	@Override
	public T previous() {
		// TODO Auto-generated method stub
		T retornado = null;
		actual = actual.getAnterior();
		retornado = actual.getElemento();
		return retornado;
	}

	@Override
	public int previousIndex() {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException();
	}

	@Override
	public void remove() {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException();
		
	}

	@Override
	public void set(T arg0) {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException();
		
	}
	
}
