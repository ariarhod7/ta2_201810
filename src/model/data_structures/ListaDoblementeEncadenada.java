package model.data_structures;

import java.util.Comparator;

import model.vo.Taxi;

public class ListaDoblementeEncadenada<T extends Comparable<T>> implements LinkedList<T> {

	/**
	 * Atributos de la Lista doblemente encadenada
	 */
	private int cantidadElementos;
	private NodoListaDoble<T> primerNodo;

	/**
	 * Contructor de la lista doblemente encadenada
	 */
	public ListaDoblementeEncadenada() {
		primerNodo = null;
		cantidadElementos = 0;
	}

	/**
	 * Agrega un elemento a la lista doblemente encadenada
	 * @exception Si el elemetno ingresado como parametro es nulo
	 */
	@Override
	public boolean add(T elemento) throws Exception {
		// TODO Auto-generated method stub
		if ( elemento == null) {
			throw new Exception ("El elemento en nulo");
		}
		else {
			if ( primerNodo == null) {
				NodoListaDoble<T> agregar = new NodoListaDoble<T>(elemento);
				primerNodo = agregar;
				cantidadElementos++;
				return true;
			}
			else {
				NodoListaDoble<T> ultimo = darNodo(cantidadElementos-1);
				NodoListaDoble<T> agregar = new NodoListaDoble<T>(elemento);
				ultimo.setSiguiente(agregar);
				agregar.setAnterior(ultimo);
				cantidadElementos++;
				return true;
			}
		}
	}

	/**
	 * Elimina de la lista el elemento que entra como parametro
	 */
	@Override
	public boolean remove(T elemento) {
		// TODO Auto-generated method stub
		boolean retornado = false;
		if ( primerNodo != null) {
			NodoListaDoble<T> anterior = primerNodo;
			NodoListaDoble<T> actual = primerNodo.getSiguiente();
			if ( primerNodo.getElemento().compareTo(elemento) == 0) {
				retornado = true;
				primerNodo = actual;
				cantidadElementos--;
			}
			else {
				while (actual != null && !retornado) {
					if (actual.getElemento().compareTo(elemento) == 0) {
						NodoListaDoble<T> quitar = actual.getSiguiente();
						anterior.setSiguiente(quitar);
						cantidadElementos--;
						retornado = true;
					}
					else {
						anterior = anterior.getSiguiente();
						actual = actual.getSiguiente();
					}
				}
			}
		}
		return retornado;

	}

	/**
	 * Obtiene de la lista el elemento entrado por parametro
	 * NULL si no lo encuentra
	 * @return Objeto ingrasado como parametro
	 */
	@Override
	public T get(T elemento) {
		// TODO Auto-generated method stub
		T retornado = null;
		NodoListaDoble<T> actual = primerNodo;
		while ( actual != null ) {
			if ( actual.getElemento().compareTo(elemento) == 0) {
				retornado = actual.getElemento();
			}
			else {
				actual = actual.getSiguiente();
			}
		}
		return retornado;
	}

	/**
	 * Retorna el tama�o de la lista
	 * @return La cantidad de elementos que contiene la lista
	 */
	@Override
	public int size() {
		// TODO Auto-generated method stub
		return cantidadElementos;
	}

	/**
	 * Retorna el objeto contenido en la poscion ingresada por parametro
	 * @return El objeto ubicado en la posicion ingresada por parametro
	 */
	@Override
	public T get(int index) {
		// TODO Auto-generated method stub
		if ( index < 0  || index >= cantidadElementos) {
			throw new IndexOutOfBoundsException("El indice esta por fuera del arreglo");
		}
		else {
			NodoListaDoble<T> nodo = darNodo(index);
			return nodo.getElemento();
		}
	}

	/**
	 * Retorna el Nodo ubicado en la posciion ingresada por parametro
	 * @return El Nodo ubicado en la posicion ingesada por parametro
	 */
	@Override
	public NodoListaDoble<T> darNodo(int index) {
		// TODO Auto-generated method stub
		if ( index < 0 || index >= cantidadElementos) {
			throw new IndexOutOfBoundsException("El indice esta por fuera ");
		}
		else {
			NodoListaDoble<T> actual = primerNodo;
			int pos = 0;
			while ( actual != null && pos<index) {
				actual = actual.getSiguiente();
				pos++;
			}
			return actual;
		}

	}

	/**
	 * Retorna true si el objeto ingreasdo por parametro esta en la lista
	 * False de lo contrario
	 */
	@Override
	public boolean contains(T ob) {
		// TODO Auto-generated method stub
		if ( cantidadElementos > 0) {
			boolean retornado = false;
			NodoListaDoble<T> primero = primerNodo;
			while ( primero != null && !retornado) {
				if (primero.getElemento().compareTo(ob) == 0) {
					retornado = true;
				}
				else {
					primero = primero.getSiguiente();
				}
			}
			return retornado;
		}
		else {
			return false;
		}
	}


}
