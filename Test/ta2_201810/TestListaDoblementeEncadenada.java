package ta2_201810;

import model.data_structures.ListaDoblementeEncadenada;
import model.data_structures.NodoListaDoble;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class TestListaDoblementeEncadenada {
	
	private ListaDoblementeEncadenada<Integer> lista;
	private NodoListaDoble<Integer> nodo;
	
	@Before
	public void setUp() {
		lista = new ListaDoblementeEncadenada<Integer> ();
	}
	
	/**
	 * Prueba los metodos de la clase ListaDoblementeEncadenada
	 * 
	 * @throws Exception
	 */
	@Test
	public void pruebaLista() throws Exception {
		//Caso1: La lista deberia inicializarse con el tama�o en cero
		assertEquals("El tama�o no es el esperado", lista.size(), 0,0);
		lista.add(2);
		lista.add(1);
		lista.add(18);
		//Caso2: Si a la lista se le agregan los elemenos anteriores el tama�o no deberia ser cero
	    //el metodo get deberia ser capaz de obtener los datos, y los elementos deben estar agregados
		//Se prueba el metodo size(), get(int index) y add(T elemento)
		assertEquals("El tama�o no es el esperado" , lista.size(), 3 , 0);
		assertEquals("El resultado no es el esperado", lista.get(0), 2 , 0);
		assertEquals("El resultado no es el esperado", lista.get(1), 1,0);
		assertEquals("El resultado no es el esperado", lista.get(2), 18,0);
		//Caso3: Se va a remover un elemento de la lista. si el metodo funciona bien
		//el tama�o deberia descender en 1 y el elemento no deberia existir con lo cual
		//Se prueba el metodo contains(T elemento), remove(T elemento) y size()
		lista.remove(1);
		assertEquals("El tama�o no es el esperado", lista.size(), 2,0);
		assertFalse("No es el booleano esperado", lista.contains(1));
		//Caso4: creo un nodo de las lista con el mismo elemento del nodo que voy a extraer
		//cuando compare el elemento que poseen deberina ser el mismo. Prueba del metodo darNodo(int index)
		nodo = new NodoListaDoble<Integer> (2);
		assertEquals("El resultado no es el esperado" , lista.darNodo(0).getElemento(), nodo.getElemento(), 0);
	}
	
	

}
